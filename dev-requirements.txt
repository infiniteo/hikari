async-timeout~=3.0.1
coverage~=5.1
nox==2020.5.24
mock~=4.0.2
pytest==5.4.3
pytest-asyncio==0.12.0
pytest-cov==2.10.0
pytest-testdox==1.2.1
virtualenv==20.0.23

